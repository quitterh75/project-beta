import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/addsalesperson">Add a Sales Person</NavLink> </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/salespersons">Sales people</NavLink> </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/addcustomer">Add a Customer</NavLink> </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink> </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/addsale">Add a Sale</NavLink> </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales </NavLink> </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/saleshistory">Sales History</NavLink> </li>

          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="technicians/create/">Add a Technician</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="technicians/">Technicians</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="appointments/create/">Create a Service Appointment</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="appointments/">Service Appointments</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="appointments/history/">Service History</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="automobiles/list">Automobiles</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="automobiles/create/">Create an Automobile</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="models/list">Models</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="models/create/">Create a Model</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="manufacturers/list">Manufacturers</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="manufacturers/create/">Add a Manufacturer</NavLink>
          </li>



          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
