import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import CustomerForm from "./Sales/CustomerForm";
import ListCustomers from "./Sales/CustomerList";
import SalesPersonForm from "./Sales/SalespersonForm";
import ListSalesPeople from "./Sales/SalespersonList";
import SaleForm from "./Sales/SaleForm";
import ListSales from "./Sales/SalesList";
import SalesHistory from "./Sales/SalesHistory";
import TechnicianForm from './Service/TechForm';
import TechnicianList from './Service/TechList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentList from './Service/ServiceAppointments';
import AppointmentHistoryList from './Service/ServiceHistory';
import ManufacturersList from './Inventory/ManufacturerList';
import AutomobileList from './Inventory/AutoList';
import AutomobileForm from './Inventory/CreateAutoForm';
import ModelsList from './Inventory/ModelsList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import VehicleModelForm from './Inventory/VehicleModelForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="addsalesperson/" element={<SalesPersonForm />} />
          <Route path="salespersons/" element={<ListSalesPeople />} />
          <Route path="addcustomer/" element={<CustomerForm />} />
          <Route path="customers/" element={<ListCustomers />} />
          <Route path="addsale/" element={<SaleForm />} />
          <Route path="sales/" element={<ListSales />} />
          <Route path="saleshistory/" element={<SalesHistory />} />
          <Route path="technicians/create/" element={<TechnicianForm />} />
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="appointments/create/" element={<AppointmentForm />} />
          <Route path="appointments/" element={<AppointmentList />} />
          <Route path="appointments/history/" element={<AppointmentHistoryList />} />
          <Route path="models/create/" element={<VehicleModelForm />} />
          <Route path="manufacturers/create/" element={<ManufacturerForm />} />
          <Route path="automobiles/create/" element={<AutomobileForm />} />
          <Route path="manufacturers/list/" element={<ManufacturersList />} />
          <Route path="automobiles/list/" element={<AutomobileList />} />
          <Route path="models/list/" element={<ModelsList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
