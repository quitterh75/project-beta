# CarCar

Team:

- Hudson Quitter - Service Microservice
- Thira Busalee(Mave) - Sales Microservice

## How to set up to view the site

**Please ensure you have Git, Docker, and Node.js 18.2 or above**

1. Please fork this repository https://gitlab.com/quitterh75/project-beta

2. Clone this repository onto your local machine. Use this command to clone: git clone https://gitlab.com/quitterh75/project-beta.git

3. Change your directory to the project directory. This can be done with this command: cd project-beta

4. Build and run the project using these commands: docker volume create beta-data, docker-compose build, docker-compose up

5. Once all containers are running and operational open your browser(google chrome preferred) to: http://localhost:3000/

6. The project should be in full view. You may test as you wish.

## Design

This project is made up of 3 microservices that interact with one another

1. Inventory
2. Sales
3. Services

## Navigation

The 'http://localhost:3000' hosts the server and serves as the homepage for this project. Once you enter the website you will see a navigation bar. The URLs and features links as shown:

##Automobiles
List automobiles: http://localhost:3000/automobiles/list
Create automobiles: http://localhost:3000/automobiles/create/

##Manufacturer
List Manufacturers: http://localhost:3000/manufacturers/list
Create Manufacturer: http://localhost:3000/manufacturers/create/

##Models
Create a Model: http://localhost:3000/models/create/
Models List: http://localhost:3000/models/list

##Salesperson
Create Salesperson: http://localhost:3000/salesperson/create
Salespeople: http://localhost:3000/salesperson/list
Sales: http://localhost:3000/salesrecord

##Customer
Create Customer: http://localhost:3000/customers/create
Customer List: http://localhost:3000/customers/list

##Sale
Create Sales Record: http://localhost:3000/salesrecord/create
Salesperson History: http://localhost:3000/salesrecord/history


##Diagram
![Alt text](image.png)
## Service microservice

My service will consist of 2 models and one valueobject. Appointment, Technician and AutomobileVO. A foreign key was put into place in the appointment model to access the technician model. A poller was created to make objects for my service by polling the inventory api. During back-end development in the views.py file for the service api functions were created in conjunction with each model. These functions were made to make sure the data was stored and created in the appropiate manner. In the urls.py file in the service api paths were made for the functions in views.py. Insomnia was used to test if the paths and functions were performing up to par.

## Service RESTful API calls:

##Feature: Create Technician; Method: POST; URL: http://localhost:8080/api/technicians/
##Feature: Create Service; Method: appointment POST; URL: http://localhost:8080/api/appointments/
##Feature: Create Service; Method: appointment GET; URL: http://localhost:8080/api/appointments/
##Feature: List Technicians; Method: GET; URL: http://localhost:8080/api/technicians/
##Feature: List Appointments; Method: GET; URL: http://localhost:8080/api/appointments/
##Feature: View Appointments; Method: GET; URL: http://localhost:8080/api/appointments/
##Feature: View Service History; Method: POST; URL: http://localhost:8080/api/appointments/

## Sales microservice

The sales microservice will be use to create salespeople and customers that interact together to create sales of the automobile. There are a few feature such as listing all the automobiles, filthering out the sales made by the salespeople.
The sales microservice is able to intergate and poller from the inventory api to receive the data in sales section. The sales microservice included four pieces of models inside. They are AutomobileVO, Customer, Salesperson and Sale. AutomobileVO contain VIN, color, and year of the automobile model which is polled from the Inventory microservice. Customer contained first name, last name, address and phone number. Salesperson contain employee number and full name. Lastly Sales contain full record making new inventory, customer info, salesperson info, automobile and the price of the vehicle.

__Sales Port: 8090__

## Sales RESTful API calls:
Feature: Create a customer; Method: POST; URL: http://localhost:8090/api/customers/|
Feature: List customers; Method: GET; URL: http://localhost:8090/api/customers/|
Feature: Create a salesperson; Method: POST; URL: http://localhost:8090/api/salespeople/|
Feature: List salespeople; Method: GET; URL: http://localhost:8090/api/salespeople/|
Feature: Sales record; Method: GET; URL: http://localhost:8090/api/sales/|
Feature: Create a sale; Method: POST; URL: http://localhost:8090/api/sales/|
Feature: Salesperson history; Method: GET; URL: http://localhost:8090/api/sales/|
