from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.PositiveIntegerField()

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.employee_id})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=50)
    status = models.CharField(max_length=100, default="Created")
    is_vip = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def cancel(self):
        self.status = "Cancelled"
        self.save()

    def finish(self):
        self.status = "Finished"
        self.save()

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return self.reason
