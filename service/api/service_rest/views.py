from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import TechnicianListEncoder, AppointmentListEncoder
from .models import AutomobileVO, Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Technician cannot be created"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.filter(id=id)
        return JsonResponse(
            {"technician": technician}, encoder=TechnicianListEncoder, safe=False
        )
    else:
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse({"message": "Deleted technician"}, status=200)

        except Technician.DoesNotExist:
            response = JsonResponse(
                {"Message": "Technician has not yet been created"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            try:
                technician_id = content["technician"]
                technician = Technician.objects.get(id=technician_id)
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "There is no technician with that name"}, status=400
                )
            vin = content["vin"]
            if AutomobileVO.objects.filter(vin=vin).count() == 1:
                content["is_vip"] = True
            appointment = Appointment.objects.create(**content)
            return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)
        except Exception:
            response = JsonResponse(
                {"message": "Appointment cannot be created"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.filter(id=id)
        return JsonResponse(
            {"appointment": appointment}, encoder=AppointmentListEncoder, safe=False
        )
    else:
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment has been deleted"},
                status=200,
            )
        except Exception:
            response = JsonResponse({"Message": "Appointment does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.cancel()
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Appointment cannot be cancelled"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.finish()
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)
    except Exception:
        response = JsonResponse({"message": "Appointment cannot be completed"})
        response.status_code = 404
        return response
